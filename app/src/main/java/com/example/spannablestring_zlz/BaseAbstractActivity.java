package com.example.spannablestring_zlz;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

abstract public class BaseAbstractActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getLayoutId() != 0) {
            LayoutInflater inflater = LayoutInflater.from(this);
            View baseView = inflater.inflate(getLayoutId(), null);
            setContentView(baseView);
        }
        init();
    }


    abstract protected int getLayoutId();

    abstract protected void init();

    protected BaseAbstractActivity getActivity() {
        return this;
    }
}
