package com.example.spannablestring_zlz;

import android.graphics.Color;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;

public class SpannableStringUtils {


    public static void dealWithSpannableString(String text, TextView textView, OnSpannableStringClickListener onSpannableStringClickListener) {

        String[] arrays = text.split("##");
        final int startIndex = arrays[0].length();
        final int endIndex = arrays[0].length() + arrays[1].length();
        final String title = text.replace("##", "");
        SpannableString spannableString = new SpannableString(title);
        spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#4089FF")), startIndex, endIndex, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                if (onSpannableStringClickListener != null) {
                    onSpannableStringClickListener.onSpannableStringClick();
                }
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#4089FF"));
                ds.setUnderlineText(false);
            }
        }, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

//        //下划线
//        spannableString.setSpan(new UnderlineSpan(), startIndex, endIndex, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        textView.setText(spannableString);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

}



