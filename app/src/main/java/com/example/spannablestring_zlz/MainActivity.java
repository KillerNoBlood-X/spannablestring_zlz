package com.example.spannablestring_zlz;

import android.content.Intent;
import android.net.Uri;
import android.widget.TextView;

public class MainActivity extends BaseAbstractActivity {


    private TextView tvClickMe;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void init() {
        tvClickMe = findViewById(R.id.id_tv_click_me);
        SpannableStringUtils.dealWithSpannableString(getString(R.string.string_click_me), tvClickMe, new OnSpannableStringClickListener() {
            @Override
            public void onSpannableStringClick() {
                openExternalBrowser(getString(R.string.string_my_blog_url));
            }
        });
    }

    /**
     * 通过外部浏览器打开
     *
     * @param url
     */
    private void openExternalBrowser(String url) {
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
}